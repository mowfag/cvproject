$(function(){
    $('.nLink').on('click', function(e){
        $(".nLink").removeClass("active");
        $(this).addClass("active");

        let x = $(this).attr("name");
        let s = $("section");
        if(x == 4){
            s.show().parent().show();

            $(".slideR").removeClass("slideR");
            $('main').addClass("slideR");

        } else if(!s.eq(x).is(s.filter('.slideR'))){
            s.hide().parent().hide();
            s.eq(x).show().parent().show();

            $(".slideR").removeClass("slideR");
            s.eq(x).addClass("slideR");
        } 
    });

    $(window).on('click', function(e) {
        $(e.target).is('.fa-envelope-open-text') ? $('.fWrapper').show() : 1==1;
        $(e.target).is('.fWrapper') ? $('.fWrapper').hide() : 1==1;
    });
    
    $("form").on('submit', function(e){
        e.preventDefault();
        Email.send({
            SecureToken : "e856884c-943e-495b-95f1-be15a0817908",
            To : "mohamedalfaki82@gmail.com",
            From : "mohamedalfaki82@gmail.com",
            Subject : $('[name="obj"]').val(),
            Body : `from ${$('[name="fname"]').val()} ${$('[name="lname"]').val()} Email ${$('[name="eml"]').val()} Message ${$('[name="msg"]').val()}` 
        }).then(
            message => alert(message)
        );
    });
});